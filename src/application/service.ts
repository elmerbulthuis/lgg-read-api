import pg from "pg";
import { Config } from "./config.js";

export interface Services {
    pgPool: pg.Pool;
}

export function createServices(
    config: Config,
): Services {
    const pgPool = new pg.Pool({
        connectionString: config.pgUri.toString(),
    });

    return {
        pgPool,
    };
}
