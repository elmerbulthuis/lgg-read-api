import * as readApi from "@latency.gg/lgg-read-oas";
import { Context } from "./context.js";

export type Server = readApi.Server;

export function createServer(
    context: Context,
) {
    const server = new readApi.Server({
        baseUrl: context.config.endpoint,
        signal: context.signal,
    });

    return server;
}
